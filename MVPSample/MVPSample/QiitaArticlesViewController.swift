//
//  QiitaArticlesViewController.swift
//  MVPSample
//
//  Created by kou yamamoto on 2021/02/21.
//

import UIKit

class QiitaArticlesViewController: UIViewController {
    
    @IBOutlet weak var articleTableview: UITableView!
    private var qiitaArticlePresenter: QiitaArticlePresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        articleTableview.delegate = self
        articleTableview.dataSource = self
        qiitaArticlePresenter = QiitaArticlePresenter(view: self, model: QiitaArticleModel())
        qiitaArticlePresenter.fetchQiitaArticles()
    }
}

extension QiitaArticlesViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return qiitaArticlePresenter.numberOfArticles
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleCell", for: indexPath)
        guard let article = qiitaArticlePresenter.article(forRow: indexPath.row) else { return cell }
        cell.textLabel?.text = article.title
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        qiitaArticlePresenter.didSelectRow(at: indexPath)
    }
}

extension QiitaArticlesViewController: QiitaArticlePresenterOutput {
    func updateArticles(_ article: [Article]) {
        articleTableview.reloadData()
    }
    
    func transitionToArticleDetail(url: String) {
        let articleDetailViewController = self.storyboard?.instantiateViewController(identifier: "ArticleDetailViewController") as! WebViewController
        articleDetailViewController.url = url
        navigationController?.pushViewController(articleDetailViewController, animated: true)
    }
}
