//
//  QiitaArticlePresenter.swift
//  MVPSample
//
//  Created by kou yamamoto on 2021/02/21.
//

import Foundation
import UIKit

protocol QiitaArticlePresenterInput {
    var numberOfArticles: Int { get }
    func didSelectRow(at indexPath: IndexPath)
}

protocol QiitaArticlePresenterOutput {
    func updateArticles(_ article: [Article])
    func transitionToArticleDetail(url: String)
}

class QiitaArticlePresenter: QiitaArticlePresenterInput {
    
    private var articles: [Article] = []
    private var view: QiitaArticlePresenterOutput!
    private var model: QiitaArticleModelInput!
    
    var numberOfArticles: Int { return articles.count }
    
    init(view: QiitaArticlePresenterOutput, model: QiitaArticleModelInput) {
        self.view = view
        self.model = model
    }
    
    func article(forRow row: Int) -> Article? {
        guard row < articles.count else { return nil }
        return articles[row]
    }
    
    func didSelectRow(at indexPath: IndexPath) {
        guard let article = article(forRow: indexPath.row) else { return }
        view.transitionToArticleDetail(url: article.url)
    }
    
    func fetchQiitaArticles() {
        model.fetchQiitaArticles { [weak self] result in
            switch result {
            case .success(let articles):
                self?.articles = articles
                DispatchQueue.main.async { self?.view.updateArticles(articles) }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}
