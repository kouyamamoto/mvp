//
//  QiitaArticleModel.swift
//  MVPSample
//
//  Created by kou yamamoto on 2021/02/21.
//

import Foundation
import UIKit
import Alamofire

protocol QiitaArticleModelInput {
    func fetchQiitaArticles(completion: @escaping (Result<[Article], Error>) -> ())
}

class QiitaArticleModel: QiitaArticleModelInput {
    
    var api = "https://qiita.com/api/v2/items"
    private var articles: [Article] = []
    
    func fetchQiitaArticles(completion: @escaping (Result<[Article], Error>) -> ()) {
        AF.request(api).responseJSON { response in
            
            switch response.result {
            case .success:
                guard let data = response.data else { return }
                do {
                    self.articles = try JSONDecoder().decode([Article].self, from: data)
                    completion(.success(self.articles))}
                catch {}
                
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
