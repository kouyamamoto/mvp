//
//  QiitaAPI.swift
//  MVPSample
//
//  Created by kou yamamoto on 2021/02/21.
//

import Foundation


class QiitaApi {
    
}

enum Api: String {
    case search = "https://qiita.com/api/v2/items"
    
    var url: String {
        return Api.search.rawValue
    }
}
