//
//  WebViewController.swift
//  MVPSample
//
//  Created by kou yamamoto on 2021/02/21.
//

import UIKit
import WebKit

class WebViewController: UIViewController {
    
    @IBOutlet weak var webView: WKWebView!
    var url: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        guard let url = url else { return }
        setup(url: url)
    }
    
    private func setup(url: String) {
        guard let url = URL(string: url) else { return }
        let request = URLRequest(url: url)
        webView.load(request)
    }
}
