//
//  Article.swift
//  MVPSample
//
//  Created by kou yamamoto on 2021/02/21.
//

import Foundation
import UIKit

struct Article: Codable {
    let title: String
    let url: String
    
    init(title: String, url: String) {
        self.title = title
        self.url = url
    }
}



